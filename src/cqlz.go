package main

import (
	"os"
	"strconv"

	"github.com/gocql/gocql"
)

type Trans struct {
	Uid         gocql.UUID
	Type        string
	FromAddress string
	ToAddress   string
	Amount      int
	Digsig      string
	Timestamp   int64
}

var Session *gocql.Session

func initCStarSession() {
	cluster := gocql.NewCluster(cassandraConfig.host)
	cluster.Port = port(cassandraConfig.port)
	cluster.Keyspace = cassandraConfig.keyspace
	cluster.Consistency = consistancy(cassandraConfig.consistancy)

	s, err := cluster.CreateSession()
	if err != nil {
		println("Error cassandra session:", err.Error())
		os.Exit(1)
	}
	Session = s
}

func clearCStarSession() {
	Session.Close()
}

func createTrans(trans *Trans) error {
	insert := func(table string) error {
		q := "INSERT INTO " + table + ` (
                uid,
                type,
                from_address,
                to_address,
                amount,
                digsig,
                timestamp
            )
            VALUES (?, ?, ?, ?, ?, ?, ?)`
		err := Session.Query(q,
			trans.Uid,
			trans.Type,
			trans.FromAddress,
			trans.ToAddress,
			trans.Amount,
			trans.Digsig,
			trans.Timestamp).Exec()
		if err != nil {
			println(err.Error())
		}

		return err
	}

	// insert to both trans and transactions
	insert("trans")
	insert("transactions")

	return nil
}

func isDoubleSpend(from string, uid string) bool {
	// parse cid and get uuid
	uuid, err := gocql.ParseUUID(uid)
	if err != nil {
		println(err.Error)
		return true
	}

	m := map[string]interface{}{}
	q := `
        SELECT id FROM trans
            WHERE from=?
            AND uid=?
        LIMIT 1
        ALLOW FILTERING
    `
	itr := Session.Query(q, from, uuid).Consistency(gocql.One).Iter()
	for itr.MapScan(m) {
		return true
	}

	return false
}

func port(p string) int {
	i, err := strconv.Atoi(p)
	if err != nil {
		return 9042
	}

	return i
}

func consistancy(c string) gocql.Consistency {
	gc, err := gocql.MustParseConsistency(c)
	if err != nil {
		return gocql.All
	}

	return gc
}

func uuid() gocql.UUID {
	return gocql.TimeUUID()
}

func uuidstr() string {
	return gocql.TimeUUID().String()
}

func toUuid(uid string) (gocql.UUID, error) {
	return gocql.ParseUUID(uid)
}
