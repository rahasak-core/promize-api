package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

type Promize struct {
	Uid         string
	Type        string
	FromAddress string
	ToAddress   string
	Amount      int
	Digsig      string
}

type Status struct {
	Uid  string
	Code string
}

func initHttpz() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/promize", promize).Methods("POST")
	r.Path("/metrics").Handler(promhttp.Handler())

	// start server
	err := http.ListenAndServe(":7070", r)
	if err != nil {
		println(err.Error)
		os.Exit(1)
	}
}

func promize(w http.ResponseWriter, r *http.Request) {
	// read body
	b, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	println(string(b))

	// unmarshel json
	// then promize -> trans
	var p Promize
	json.Unmarshal(b, &p)
	t := toTrans(p)

	// TODO check double spend

	// TODO call core bank api

	// create trans
	createTrans(t)

	// created response
	senzResponse(w, p.Uid, "201")
}

func senzResponse(w http.ResponseWriter, uid string, code string) {
	status := Status{
		Uid:  uid,
		Code: code,
	}
	j, _ := json.Marshal(status)
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(j))
}
