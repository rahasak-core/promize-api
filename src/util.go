package main

import (
	"strconv"
	"time"
)

func uid() string {
	t := time.Now().UnixNano() / int64(time.Millisecond)
	return config.senzieName + strconv.FormatInt(t, 10)
}

func timestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func toTrans(promize Promize) *Trans {
	trans := &Trans{}
	uid, _ := toUuid(promize.Uid)
	trans.Uid = uid
	trans.Type = promize.Type
	trans.FromAddress = promize.FromAddress
	trans.ToAddress = promize.ToAddress
	trans.Amount = promize.Amount
	trans.Digsig = promize.Digsig
	trans.Timestamp = timestamp()

	return trans
}
